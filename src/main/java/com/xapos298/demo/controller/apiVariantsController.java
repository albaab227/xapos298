package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.VariantsRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class apiVariantsController {

	@Autowired
	public VariantsRepository variantsRepository;

	@GetMapping("variants")
	public ResponseEntity<List<Variants>> getAllVariants() {
		try {
			List<Variants> ListVariants = this.variantsRepository.findByIsActive(true);
			return new ResponseEntity<List<Variants>>(ListVariants, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@PostMapping("variants/add")
	public ResponseEntity<Object> saveVariants(@RequestBody Variants variants) {
		variants.setCreateBy("user1");
		variants.setCreateDate(new Date());
		Variants variantsData = this.variantsRepository.save(variants);
		if (variantsData.equals(variants)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variants/{id}")
			public ResponseEntity<List<Variants>> getVariantsById(@PathVariable("id") Long id){
				try {
					Optional<Variants> variants = this.variantsRepository.findById(id);
					if(variants.isPresent()) {
						ResponseEntity rest = new ResponseEntity<>(variants,HttpStatus.OK);
						return rest;
					}else {
						return ResponseEntity.notFound().build();
					}
					}catch(Exception e) {
						return new ResponseEntity<List<Variants>>(HttpStatus.NO_CONTENT);
					}	
			}

	@PutMapping("edit/variants/{id}")
	public ResponseEntity<Object> editVariants(@PathVariable("id") Long id, @RequestBody Variants variants) {
		Optional<Variants> variantsData = this.variantsRepository.findById(id);

		if (variantsData.isPresent()) {
			variants.setId(id);
			variants.setModifyBy("user1");
			variants.setModifyDate(Date.from(Instant.now()));
			variants.setCreateBy(variantsData.get().getCreateBy());
			variants.setCreateDate(variantsData.get().getCreateDate());
			this.variantsRepository.save(variants);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}

	}
	
	
	@PutMapping("delete/variants/{id}")
	public ResponseEntity<Object> deleteVariants(@PathVariable("id") Long id) {

		Optional<Variants> variantsData = this.variantsRepository.findById(id);
		
		if (variantsData.isPresent()) {
			
			Variants variant = new Variants();
			variant.setId(id);
			variant.setIsActive(false);
			variant.setModifyBy("user1");
			variant.setModifyDate(new Date());
			variant.setCreateBy(variantsData.get().getCreateBy());
			variant.setCreateDate(variantsData.get().getCreateDate());
			variant.setVariantsInitial(variantsData.get().getVariantsInitial());
			variant.setVariantsName(variantsData.get().getVariantsName());
			variant.setCategoryId(variantsData.get().getCategoryId());
			this.variantsRepository.save(variant);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Update Failed", HttpStatus.BAD_REQUEST);
		}
	}

	
	
	@GetMapping("variantsbycategory/{id}")
	public ResponseEntity<List<Variants>> getVariantsByCategory(@PathVariable("id") Long id){
		try {
			List<Variants> ListVariants = this.variantsRepository.findByCategoryId(id);
			return new ResponseEntity<List<Variants>>(ListVariants, HttpStatus.OK);
			
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
			
	}
	
	
	@GetMapping("variants/search/{keyword}")
	public ResponseEntity<List<Variants>> getSearchVariants(@PathVariable ("keyword") String keyword){
		try {
			List<Variants> searchVariants = this.variantsRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Variants>>(searchVariants,HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}



}

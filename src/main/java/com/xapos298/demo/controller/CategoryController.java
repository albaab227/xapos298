package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {
	Boolean isEdit = false;
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexapi");
		return view;
	}

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll(); // SELECT * FROM category
		view.addObject("listCategory", listCategory);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		if (isEdit) {
			Category category2 = this.categoryRepository.findById(category.getId()).orElse(null);
			category2.setModifyDate(new Date()); // = date now / tanggal hari ini beserta hh:mm:ss
			category2.setModifyBy("user1");
			category2.setCategoryInitial(category.getCategoryInitial());
			category2.setCategoryName(category.getCategoryName());
			category2.setIsActive(category.getIsActive());
			category = category2;
			this.categoryRepository.save(category); // INSERT INTO ..... VALUES .....
			isEdit= false;
		} else if (!result.hasErrors()) {
			category.setCreateBy("user1");
			category.setCreateDate(new Date());
			this.categoryRepository.save(category);
		}
		return new ModelAndView("redirect:/category/index");
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		isEdit = true;
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null); // SELECT * FROM category WHERE = {id}
		view.addObject("category", category);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
}

package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Product;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.ProductRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class apiProductController {

	@Autowired
	public ProductRepository productRepository;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> listProduct = this.productRepository.findByIsActive(true);
			return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.setCreateBy("user1");
		product.setCreateDate(new Date());
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id") Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if(product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product,HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> editProduct(@PathVariable("id") Long id, 
		@RequestBody Product product){
			Optional<Product> productData = this.productRepository.findById(id);
			
			if(productData.isPresent()) {
				product.setId(id);
				product.setModifyBy("user1");
				product.setCreateDate(new Date());
				product.setCreateBy(productData.get().getCreateBy());
				product.setCreateDate(productData.get().getCreateDate());
				this.productRepository.save(product);
				return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
			}else {
				return ResponseEntity.notFound().build();
			}
		}
	
	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id){
		
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			
			Product product = new Product();
			product.setId(id);
			product.setIsActive(false);
			product.setModifyBy("user1");
			product.setModifyDate(new Date());
			product.setCreateBy(productData.get().getCreateBy());
			product.setCreateDate(productData.get().getCreateDate());
			product.setProductInitial(productData.get().getProductInitial());
			product.setProductName(productData.get().getProductName());
			product.setVariantsId(productData.get().getVariantsId());
			product.setProductDescription(productData.get().getProductDescription());
			product.setProductPrice(productData.get().getProductPrice());
			product.setProductStock(productData.get().getProductStock());
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Deleted Successfully",HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Update Failed", HttpStatus.BAD_REQUEST);
		}
		
		
		
	}
	
	
	@GetMapping("product/search/{keyword}")
	public ResponseEntity<List<Product>> getSearchProduct(@PathVariable ("keyword") String keyword){
		try {
			List<Product> searchProduct = this.productRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Product>>(searchProduct, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	
	

}

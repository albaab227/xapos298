package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Product;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.ProductRepository;
import com.xapos298.demo.repository.VariantsRepository;

@Controller
@RequestMapping("/product")
public class ProductController {
		Boolean isEdit = false;
		@Autowired
		private ProductRepository productRepository;
		
		@Autowired
		private VariantsRepository variantsRepository;
		
		@GetMapping("indexapi")
		public ModelAndView indexapi() {
			ModelAndView view = new ModelAndView("product/indexapi");
			return view;
		}
		
		@GetMapping("index")
		public ModelAndView index() {
			ModelAndView view = new ModelAndView("product/index");
			List<Product> ListProduct = this.productRepository.findAll();
			view.addObject("ListProduct",ListProduct);
			return view;
		}
		
		@GetMapping("addform")
		public ModelAndView addform() {
			ModelAndView view = new ModelAndView("product/addform");
			Product product = new Product();
			view.addObject("product", product);
			return view;
		}
		
		
		@PostMapping("save")
		public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
			if (isEdit) {
				Product product2 = this.productRepository.findById(product.getId()).orElse(null);
				product2.setVariantsId(product.getVariantsId());
				product2.setModifyBy("user1");
				product2.setModifyDate(new Date());
				product2.setProductInitial(product.getProductInitial());
				product2.setProductName(product.getProductName());
				product2.setIsActive(product.getIsActive());
				product = product2;
				this.productRepository.save(product);
				isEdit = false;
			} else if (!result.hasErrors()) {
				product.setCreateBy("User1");
				product.setCreateDate(new Date());
				this.productRepository.save(product);
			}
			return new ModelAndView("redirect:/variants/index");
			
		}
		
		@GetMapping("edit/{ids}")
		public ModelAndView edit(@PathVariable("ids") Long id) {
			isEdit = true;
			ModelAndView view = new ModelAndView("product/addform");
			Product product = this.productRepository.findById(id).orElse(null);
			view.addObject("product",product);

			// ambil vairants list
			List<Variants> listVariants = this.variantsRepository.findAll();
			view.addObject("listVariants", listVariants);
			return view;
		}

		@GetMapping("delete/{ids}")
		public ModelAndView delete(@PathVariable("ids") Long id) {
			if (id != null) {
				this.productRepository.deleteById(id);
			}
			return new ModelAndView("redirect:/product/index");
		}
		
}

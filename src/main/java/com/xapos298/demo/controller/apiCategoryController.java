package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class apiCategoryController {

	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> listCategory = this.categoryRepository.findByIsActive(true);
			return new ResponseEntity<List<Category>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}

	@PostMapping("category/add")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category) {
		category.setCreateBy("user1");
		category.setCreateDate(new Date());
		Category categoryData = this.categoryRepository.save(category);

		if (categoryData.equals(category)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

//	mengambil file yg diedit
	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id){
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if (category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category,HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}


//	Update Data
	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id,
			@RequestBody Category category){
		
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		
		if (categoryData.isPresent()) {
			category.setId(id);
			category.setModifyBy("user1");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreateBy(categoryData.get().getCreateBy());
			category.setCreateDate(categoryData.get().getCreateDate());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Updated Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id")Long id){
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if (categoryData.isPresent()) {
			Category category = new Category();
			category.setId(id);
			category.setIsActive(false);
			category.setModifyBy("user1");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreateBy(categoryData.get().getCreateBy());
			category.setCreateDate(categoryData.get().getCreateDate());
			category.setCategoryName(categoryData.get().getCategoryName());
			category.setCategoryInitial(categoryData.get().getCategoryInitial());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Delete Successfully",HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@GetMapping("category/search/{keyword}")
	public ResponseEntity<List<Category>> getSearchCategory(@PathVariable ("keyword") String keyword){
		try {
			List<Category> searchCategory = this.categoryRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Category>>(searchCategory, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("category/paging")
	public ResponseEntity<Map<String, Object>> getAllCategory(@RequestParam(defaultValue="0") int page,
			@RequestParam(defaultValue="5") int size){
		try {
			List<Category> category = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Category> pageTuts;
			pageTuts = this.categoryRepository.findAll(pagingSort);
			
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItem", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	
	
	

}

package com.xapos298.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetail;
import com.xapos298.demo.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class apiOrderDetailController {
	
	@Autowired
	public OrderDetailRepository orderDetailRepository;
	
	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		
		if(orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>("Save Item Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping("orderdetail/header/{headerId}")
	public ResponseEntity<List<OrderDetail>> getOrderById(@PathVariable("headerId") Long id){
		try {
			List<OrderDetail> orderDetail = this.orderDetailRepository.findByHeaderId(id);
				ResponseEntity rest = new ResponseEntity<>(orderDetail, HttpStatus.OK);
				return rest;
		}catch (Exception e) {
			return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	
	
	

}

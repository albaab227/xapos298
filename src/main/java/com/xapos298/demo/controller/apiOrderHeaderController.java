package com.xapos298.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderHeaders;
import com.xapos298.demo.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class apiOrderHeaderController {
		
	
	@Autowired
	public OrderHeaderRepository orderHeaderRepository;
	
	@PostMapping("orderheader/add")
	public ResponseEntity<Object> createReference(@RequestBody OrderHeaders orderHeader){
		
		String timeDec = System.currentTimeMillis() + "";
		orderHeader.setReference(timeDec);
		orderHeader.setAmount(0+"");
		OrderHeaders orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		if(orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
	@GetMapping("maxorderheaderid")
	public ResponseEntity<Long> getMaxOrderHeaderId(){
		try {
			Long maxId = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(maxId,HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@PutMapping("orderheader/done")
	public ResponseEntity<Object> doneProccess(@RequestBody OrderHeaders ordersHeaders){
		Long id = ordersHeaders.getId();
		
		Optional<OrderHeaders> orderHeaderData = this.orderHeaderRepository.findById(id);
		if(orderHeaderData.isPresent()) {
			ordersHeaders.setId(id);
			this.orderHeaderRepository.save(ordersHeaders);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	
	@GetMapping("listorderheader")
	public ResponseEntity <List<OrderHeaders>> getAllOrderHeaders(){
		try {
			List<OrderHeaders> orderDetailHeader = this.orderHeaderRepository.findByAmounts();
			return new ResponseEntity<List<OrderHeaders>>(orderDetailHeader, HttpStatus.OK);
		}catch (Exception e) {
				return new ResponseEntity<List<OrderHeaders>>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	
		
	
	
}

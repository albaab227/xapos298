package com.xapos298.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="orders_detail")
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "id")
	private long id;
	
	@Column(name= "header_id")
	private long headerId;
	
	@Column(name= "product_id")
	private long productId;
	
	@Column(name= "quantity")
	private int quantity;
	
	@Column(name= "price")
	private long price;
	
	@ManyToOne
	@JoinColumn(name = "header_id", insertable = false, updatable = false)
	public OrderHeaders ordersHeaders;
	
	@ManyToOne
	@JoinColumn(name = "product_id", insertable = false, updatable = false)
	public Product product;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getHeaderId() {
		return headerId;
	}

	public void setHeaderId(long headerId) {
		this.headerId = headerId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public OrderHeaders getOrdersHeaders() {
		return ordersHeaders;
	}

	public void setOrdersHeaders(OrderHeaders ordersHeaders) {
		this.ordersHeaders = ordersHeaders;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	

}

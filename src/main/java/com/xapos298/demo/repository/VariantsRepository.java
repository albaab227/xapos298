package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variants;

public interface VariantsRepository extends JpaRepository<Variants, Long>{
	
	
	//menggunakan jpaRepository
	List<Variants> findByIsActive(Boolean isActive);
	
	List <Variants> findByIsActiveAndCreateBy(Boolean isActive, String user);
	
	//Menggunakan Query Native
	@Query(value = "SELECT * FROM variants WHERE is_active = ?1 AND created_by = ?2", nativeQuery = true)
	List<Variants> findByVariants(Boolean isActive, String user);
	
	List<Variants> findByCategoryId(Long categoryId);
	
	
	@Query("FROM Variants WHERE lower(variantsName) LIKE lower(concat('%', ?1, '%' )) AND isActive=true")
	List<Variants> searchByKeyword(String keyword);
	
	
}
package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.OrderHeaders;

public interface OrderHeaderRepository extends JpaRepository<OrderHeaders, Long>{

		@Query("SELECT MAX(id) FROM OrderHeaders")
		public Long findByMaxId();
		
		
		@Query(value = "SELECT * FROM orders_headers WHERE NOT amount = '0'", nativeQuery=true)
		List<OrderHeaders> findByAmounts ();
		
		
}
